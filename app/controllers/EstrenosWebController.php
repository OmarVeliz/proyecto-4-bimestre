<?php

class EstrenosWebController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $nerds = Estrenos::all();

        return View::make('estrenos.index')
            ->with('Estrenos', $nerds);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('estrenos.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $rules = array(
            'cartelera_id' => 'required',
            'fecha_inicio' => 'required',
            'fecha_final' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('EstrenosW/create')
                ->withErrors($validator);
            //->withInput(Input::except('password'));
        } else {
            // store
            $nerds = new Estrenos();
            $nerds->cartelera_id = Input::get('cartelera_id');
            $nerds->fecha_inicio = Input::get('fecha_inicio');
            $nerds->fecha_final = Input::get('fecha_final');
            $nerds->save();

            // redirect
            Session::flash('message', 'Successfully created Product!');
            return Redirect::to('EstrenosW');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $form = Estrenos::find($id);

        // show the view and pass the nerd to it
        return View::make('estrenos.show')
            ->with('estrenos_detail', $form);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $nerds = Estrenos::find($id);

        // show the edit form and pass the nerd
        return View::make('estrenos.edit')
            ->with('estrenos_detail', $nerds);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $rules = array(
            'cartelera_id' => 'required',
            'fecha_inicio' => 'required',
            'fecha_final' => 'required',
        );

        $validatorFormat = Validator::make(Input::all(), $rules);

        // process the login
        if ($validatorFormat->fails()) {
            return Redirect::to('EstrenosW/' . $id . '/edit')
                ->withErrors($validatorFormat);
            //->withInput(Input::except('password'));
        } else {
            // store
            $nerds = Estrenos::find($id);
            $nerds->cartelera_id = Input::get('cartelera_id');
            $nerds->fecha_inicio = Input::get('fecha_inicio');
            $nerds->fecha_final = Input::get('fecha_final');
            $nerds->save();
            // redirect
            Session::flash('message', 'Successfully created Product!');
            return Redirect::to('EstrenosW');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $nerds = Estrenos::find($id);
        $nerds->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the nerd!');
        return Redirect::to('FormatoPeliculaW');
	}


}
