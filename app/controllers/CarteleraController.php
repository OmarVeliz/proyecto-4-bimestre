<?php

class CarteleraController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
        return Cartelera::all();
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return DB::table('Cartelera')
            ->join('Pelicula', 'Cartelera.pelicula_id', '=', 'Pelicula.id')
            ->join('Sala', 'Cartelera.sala_id', '=', 'Sala.id')
            ->join('Cine','Sala.cine_id', '=', 'Cine.id')
            ->select('Pelicula.id', 'Cine.nombre')
            ->where('Cine.id','=', $id)
            ->get();

        //return Cartelera::findOrFail($id);

    }


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
