<?php

class EstrenosController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
        return DB::table('Estrenos')
            ->join('Cartelera', 'Estrenos.cartelera_id', '=', 'Cartelera.id')
            ->join('Pelicula','Cartelera.pelicula_id','=','Pelicula.id')
            ->select('Pelicula.titulo','Pelicula.sinopsis', 'Pelicula.image','Estrenos.fecha_inicio', 'Estrenos.fecha_final', 'Pelicula.trailer_url')
            ->get();
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
        return DB::table('Estrenos')
            ->join('Cartelera', 'Estrenos.cartelera_id', '=', 'Cartelera.id')
            ->join('Pelicula','Cartelera.pelicula_id','=','Pelicula.id')
            ->select('Pelicula.titulo','Pelicula.sinopsis','Estrenos.fecha_inicio', 'Estrenos.fecha_final')
            ->where('Estrenos.id','=',$id)
            ->get();
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
