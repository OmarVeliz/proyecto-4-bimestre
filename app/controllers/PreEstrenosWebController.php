<?php

class PreEstrenosWebController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $nerds = PreEstrenos::all();

        return View::make('preestreno.index')
            ->with('PreEstrenos', $nerds);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('preestreno.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $rules = array(
            'pelicula_id' => 'required',
            'fecha_Estreno' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('PreEstrenosW/create')
                ->withErrors($validator);
            //->withInput(Input::except('password'));
        } else {
            // store
            $nerds = new PreEstrenos();
            $nerds->pelicula_id = Input::get('pelicula_id');
            $nerds->fecha_Estreno = Input::get('fecha_Estreno');
            $nerds->save();

            // redirect
            Session::flash('message', 'Successfully created Product!');
            return Redirect::to('PreEstrenosW');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $form = PreEstrenos::find($id);

        // show the view and pass the nerd to it
        return View::make('preestreno.show')
            ->with('preestrenos_detail', $form);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $nerds = PreEstrenos::find($id);

        // show the edit form and pass the nerd
        return View::make('preestreno.edit')
            ->with('preestrenos_detail', $nerds);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $rules = array(
            'pelicula_id' => 'required',
            'fecha_Estreno' => 'required',
        );

        $validatorFormat = Validator::make(Input::all(), $rules);

        // process the login
        if ($validatorFormat->fails()) {
            return Redirect::to('PreEstrenosW/' . $id . '/edit')
                ->withErrors($validatorFormat);
            //->withInput(Input::except('password'));
        } else {
            // store
            $nerds = PreEstrenos::find($id);
            $nerds->pelicula_id = Input::get('pelicula_id');
            $nerds->fecha_Estreno = Input::get('fecha_Estreno');
            $nerds->save();
            // redirect
            Session::flash('message', 'Successfully created Product!');
            return Redirect::to('PreEstrenosW');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $nerds = PreEstrenos::find($id);
        $nerds->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the nerd!');
        return Redirect::to('PreEstrenosW');
	}


}
