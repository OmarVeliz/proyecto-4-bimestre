<?php

class CarteleraWebController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $nerds = Cartelera::all();

        return View::make('cartelera.index')
            ->with('Cartelera', $nerds);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('cartelera.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $rules = array(
            'sala_id' => 'required',
            'pelicula_id' => 'required',
            'formatopelicula_id' => 'required',
            'formato_lenguaje' => 'required',
            'fecha' => 'required',
            'hora' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('PeliculasW/create')
                ->withErrors($validator);
            //->withInput(Input::except('password'));
        } else {
            // store
            $nerds = new Cartelera();
            $nerds->sala_id = Input::get('sala_id');
            $nerds->pelicula_id = Input::get('pelicula_id');
            $nerds->formatopelicula_id = Input::get('formatopelicula_id');
            $nerds->formato_lenguaje = Input::get('formato_lenguaje');
            $nerds->fecha = Input::get('fecha');
            $nerds->hora = Input::get('hora');
            $nerds->save();

            // redirect
            Session::flash('message', 'Successfully created Product!');
            return Redirect::to('CarteleraW');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $form = Cartelera::find($id);

        // show the view and pass the nerd to it
        return View::make('cartelera.show')
            ->with('cartelera_detail', $form);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $nerds = Cartelera::find($id);

        // show the edit form and pass the nerd
        return View::make('cartelera.edit')
            ->with('cartelera_detail', $nerds);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $rules = array(
            'sala_id' => 'required',
            'pelicula_id' => 'required',
            'formatopelicula_id' => 'required',
            'formato_lenguaje' => 'required',
            'fecha' => 'required',
            'hora' => 'required',
        );

        $validatorFormat = Validator::make(Input::all(), $rules);

        // process the login
        if ($validatorFormat->fails()) {
            return Redirect::to('CarteleraW/' . $id . '/edit')
                ->withErrors($validatorFormat);
            //->withInput(Input::except('password'));
        } else {
            // store
            $nerds = Cartelera::find($id);
            $nerds->sala_id = Input::get('sala_id');
            $nerds->pelicula_id = Input::get('pelicula_id');
            $nerds->formatopelicula_id = Input::get('formatopelicula_id');
            $nerds->formato_lenguaje = Input::get('formato_lenguaje');
            $nerds->fecha = Input::get('fecha');
            $nerds->hora = Input::get('hora');
            $nerds->save();
            // redirect
            Session::flash('message', 'Successfully created Product!');
            return Redirect::to('CarteleraW');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $nerds = Cartelera::find($id);
        $nerds->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the nerd!');
        return Redirect::to('CarteleraW');
	}


}
