<?php

class PeliculasWebController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $nerds = Pelicula::all();

        return View::make('pelicula.index')
            ->with('Pelicula', $nerds);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('pelicula.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $rules = array(
            'titulo' => 'required',
            'sinopsis' => 'required',
            'trailer_url' => 'required',
            'image' => 'required',
            'rated' => 'required',
            'genero' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('PeliculasW/create')
                ->withErrors($validator);
            //->withInput(Input::except('password'));
        } else {
            // store
            $nerds = new Pelicula();
            $nerds->titulo = Input::get('titulo');
            $nerds->sinopsis = Input::get('sinopsis');
            $nerds->trailer_url = Input::get('trailer_url');
            $nerds->image = Input::get('image');
            $nerds->rated = Input::get('rated');
            $nerds->genero = Input::get('genero');
            $nerds->save();

            // redirect
            Session::flash('message', 'Successfully created Product!');
            return Redirect::to('PeliculasW');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $form = Pelicula::find($id);

        // show the view and pass the nerd to it
        return View::make('pelicula.show')
            ->with('pelicula_detail', $form);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $nerds = Pelicula::find($id);

        // show the edit form and pass the nerd
        return View::make('pelicula.edit')
            ->with('pelicula_detail', $nerds);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $rules = array(
            'titulo' => 'required',
            'sinopsis' => 'required',
            'trailer_url' => 'required',
            'image' => 'required',
            'rated' => 'required',
            'genero' => 'required',
        );

        $validatorFormat = Validator::make(Input::all(), $rules);

        // process the login
        if ($validatorFormat->fails()) {
            return Redirect::to('PeliculasW/' . $id . '/edit')
                ->withErrors($validatorFormat);
            //->withInput(Input::except('password'));
        } else {
            // store
            $nerds = Pelicula::find($id);
            $nerds->titulo = Input::get('titulo');
            $nerds->sinopsis = Input::get('sinopsis');
            $nerds->trailer_url = Input::get('trailer_url');
            $nerds->image = Input::get('image');
            $nerds->rated = Input::get('rated');
            $nerds->genero = Input::get('genero');
            $nerds->save();
            // redirect
            Session::flash('message', 'Successfully created Product!');
            return Redirect::to('PeliculasW');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $nerds = Pelicula::find($id);
        $nerds->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the nerd!');
        return Redirect::to('PeliculasW');
	}


}
