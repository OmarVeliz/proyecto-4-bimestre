<?php

class TipoSalaWebController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
        $nerds = TipoSala::all();

        return View::make('tipo.index')
            ->with('TipoSala', $nerds);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
        return View::make('tipo.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
        $rules = array(
            'nombre' => 'required',
            'descripcion' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('FormatoPeliculaW/create')
                ->withErrors($validator);
            //->withInput(Input::except('password'));
        } else {
            // store
            $nerds = new TipoSala();
            $nerds->nombre = Input::get('nombre');
            $nerds->descripcion = Input::get('descripcion');
            $nerds->save();

            // redirect
            Session::flash('message', 'Successfully created Product!');
            return Redirect::to('TipoSalaW');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
        $form = TipoSala::find($id);

        // show the view and pass the nerd to it
        return View::make('tipo.show')
            ->with('tipo_detail', $form);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
        $nerds = TipoSala::find($id);

        // show the edit form and pass the nerd
        return View::make('tipo.edit')
            ->with('tipo_detail', $nerds);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
        $rules = array(
            'nombre' => 'required',
            'descripcion' => 'required',
        );

        $validatorFormat = Validator::make(Input::all(), $rules);

        // process the login
        if ($validatorFormat->fails()) {
            return Redirect::to('TipoSalaW/' . $id . '/edit')
                ->withErrors($validatorFormat);
            //->withInput(Input::except('password'));
        } else {
            // store
            $nerds = TipoSala::find($id);
            $nerds->nombre = Input::get('nombre');
            $nerds->descripcion = Input::get('descripcion');
            $nerds->save();
            // redirect
            Session::flash('message', 'Successfully created Product!');
            return Redirect::to('TipoSalaW');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
        $nerds = TipoSala::find($id);
        $nerds->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the nerd!');
        return Redirect::to('TipoSalaW');
	}


}
