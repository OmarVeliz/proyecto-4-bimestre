<?php
/**
 * Created by PhpStorm.
 * User: jake
 * Date: 11/07/15
 * Time: 07:12 PM
 */

class PreEstrenos extends Eloquent {
    protected $table = 'PreEstrenos';
    public $timestamps = false;

    public function pelicula() {
        return $this ->hasOne('Pelicula');
    }

}