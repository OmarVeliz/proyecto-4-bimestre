<?php
/**
 * Created by PhpStorm.
 * User: jake
 * Date: 11/07/15
 * Time: 04:20 PM
 */

class Cartelera extends Eloquent{
    public $timestamps = false;
    protected $table = 'Cartelera';

    public function sala() {
        return $this ->hasOne('Sala');
    }

    public function pelicula() {
        return $this ->hasOne('Pelicula');
    }

    public function tipoSala() {
        return $this ->hasOne('TipoSala');
    }

}