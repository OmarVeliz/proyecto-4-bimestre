<?php
/**
 * Created by PhpStorm.
 * User: jake
 * Date: 11/07/15
 * Time: 07:07 PM
 */

class Estrenos extends Eloquent {
    protected $table = 'Estrenos';
    public $timestamps = false;

    public function cartelera() {
        return $this ->hasOne('Cartelera');
    }

}