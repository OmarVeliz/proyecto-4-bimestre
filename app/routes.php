<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::group(array('prefix' => 'api'),function() {

    Route::resource('peliculas','PeliculasController');
    Route::resource('tiposala','TipoSalaController');
    Route::resource('cines','CinesController');
    Route::resource('cartelera','CarteleraController');
    Route::resource('sala','SalaController');
    Route::resource('formatopelicula','FormatoPeliculaController');
    Route::resource('mostrarpelicula', 'mostrarPeliculasController');
    Route::resource('mostrarfunciones', 'mostrarFuncionesController');
    Route::resource('estrenos','EstrenosController');
    Route::resource('preestrenos','PreEstrenosController');

});

Route::get('/', 'cineWebController@index');
Route::resource('CineW', 'cineWebController');

Route::get('/', 'FormatoPeliculaWebController@index');
Route::resource('FormatoPeliculaW', 'FormatoPeliculaWebController');

Route::get('/', 'TipoSalaWebController@index');
Route::resource('TipoSalaW', 'TipoSalaWebController');

Route::get('/', 'SalaWebController@index');
Route::resource('SalaW', 'SalaWebController');

Route::get('/', 'PeliculasWebController@index');
Route::resource('PeliculasW', 'PeliculasWebController');

Route::get('/', 'CarteleraWebController@index');
Route::resource('CarteleraW', 'CarteleraWebController');

Route::get('/', 'EstrenosWebController@index');
Route::resource('EstrenosW', 'EstrenosWebController');

Route::get('/', 'PreEstrenosWebController@index');
Route::resource('PreEstrenosW', 'PreEstrenosWebController');

Route::get('welcome', function () {
    return View::make('welcome');
});

Route::controller('user', 'UsersController');
