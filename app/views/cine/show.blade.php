<!DOCTYPE html>
<html>
<head>
    <title>Ver</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
@if(Auth::check())
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('CineW') }}">Cines</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('CineW') }}">Ver todos los cines</a></li>
            <li><a href="{{ URL::to('CineW/create') }}">Agregar un cine</a>
        </ul>
    </nav>

    <h1>Showing {{ $cine_detail->nombre }}</h1>

    <div class="jumbotron text-center">
        <h2>{{$cine_detail->nombre }}</h2>
        <p>
            <strong>Direccion:</strong> {{ $cine_detail->direccion }}<br>
        </p>
    </div>

</div>
</body>
@else
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-danger">
                    Primero debe iniciar sesion
                </h3>
            </div>
        </div>
    </div>
@endif
</html>