<!DOCTYPE html>
<html>
<head>
    <title>Editar Cine</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
@if(Auth::check())
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('CineW') }}">Cine</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('CineW') }}">Ver todos los cines</a></li>
            <li><a href="{{ URL::to('CineW/create') }}">Agregar un cine</a>
        </ul>
    </nav>

    <h1>Edit {{ $cine_detail->nombre }}</h1>

    <!-- if there are creation errors, they will show here -->
    {{ HTML::ul($errors->all()) }}

    {{ Form::model($cine_detail, array('route' => array('CineW.update', $cine_detail->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('nombre', 'Nombre') }}
        {{ Form::text('nombre', null, array('class' => 'form-control')) }}
        {{ Form::label('direccion', 'Direccion') }}
        {{ Form::text('direccion', null, array('class' => 'form-control')) }}
        {{ Form::label('telefono', 'Telefono') }}
        {{ Form::text('telefono', null, array('class' => 'form-control')) }}
        {{ Form::label('latitud', 'Latitud') }}
        {{ Form::text('latitud', null, array('class' => 'form-control')) }}
        {{ Form::label('longitud', 'Longitud') }}
        {{ Form::text('longitud', null, array('class' => 'form-control')) }}
        {{ Form::label('hora_apertura', 'Hora apertura') }}
        {{ Form::text('hora_apertura', null, array('class' => 'form-control')) }}
        {{ Form::label('hora_cierre', 'Hora cierre') }}
        {{ Form::text('hora_cierre', null, array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Edit the Nerd!', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>
</body>
@else
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-danger">
                    Primero debe iniciar sesion
                </h3>
            </div>
        </div>
    </div>
@endif
</html>