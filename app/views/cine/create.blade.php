<!DOCTYPE html>
<html>
<head>
    <title>Agregar cine</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
@if(Auth::check())
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('CineW') }}">Cines</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('CineW') }}">Ver todos los Cines</a></li>
            <li><a href="{{ URL::to('CineW/create') }}">Agregar un Cine</a>
        </ul>
    </nav>

    <h1>Agregar un cine</h1>

    <!-- if there are creation errors, they will show here -->
    {{ HTML::ul($errors->all()) }}

    {{ Form::open(array('url' => 'CineW')) }}

    <div class="form-group">
        {{ Form::label('nombre', 'Nombre') }}
        {{ Form::text('nombre', Input::old('nombre'), array('class' => 'form-control')) }}
        {{ Form::label('direccion', 'Direccion') }}
        {{ Form::text('direccion', Input::old('direccion'), array('class' => 'form-control')) }}
        {{ Form::label('telefono', 'Telefono') }}
        {{ Form::text('telefono', Input::old('telefono'), array('class' => 'form-control')) }}
        {{ Form::label('latitud', 'Latitud') }}
        {{ Form::text('latitud', Input::old('latitud'), array('class' => 'form-control')) }}
        {{ Form::label('longitud', 'Longitud') }}
        {{ Form::text('longitud', Input::old('longitud'), array('class' => 'form-control')) }}
        {{ Form::label('hora_apertura', 'Hora_apertura') }}
        {{ Form::text('hora_apertura', Input::old('hora apertura'), array('class' => 'form-control')) }}
        {{ Form::label('hora_cierre', 'Hora_cierre') }}
        {{ Form::text('hora_cierre', Input::old('hora cierre'), array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Agregar un cine!', array('class' => 'btn btn-primary')) }}
    {{ Form::close() }}

</div>
</body>
@else
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-danger">
                    Primero debe iniciar sesion
                </h3>
            </div>
        </div>
    </div>
@endif
</html>