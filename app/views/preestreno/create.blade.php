<!DOCTYPE html>
<html>
<head>
    <title>Agregar nuevo PreEstreno</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
@if(Auth::check())
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('PreEstrenosW') }}">PreEstreno</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('PreEstrenosW') }}">Ver todos los preEstrenos</a></li>
            <li><a href="{{ URL::to('PreEstrenosW/create') }}">Agregar un nuevo preEstreno</a>
        </ul>
    </nav>

    <h1>Agregar un PreEstreno</h1>

    <!-- if there are creation errors, they will show here -->
    {{ HTML::ul($errors->all()) }}

    {{ Form::open(array('url' => 'PreEstrenosW')) }}

    <div class="form-group">
        {{ Form::label('pelicula_id', 'Pelicula_id') }}
        {{ Form::text('pelicula_id', Input::old('pelicula_id'), array('class' => 'form-control')) }}
        {{ Form::label('fecha_Estreno', 'Fecha_Estreno') }}
        {{ Form::text('fecha_Estreno', Input::old('fecha_Estreno'), array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Agregar preEstreno!', array('class' => 'btn btn-primary')) }}
    {{ Form::close() }}

</div>
</body>
@else
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-danger">
                    Primero debe iniciar sesion
                </h3>
            </div>
        </div>
    </div>
@endif
</html>