@include("../inc/header")

@if(Auth::check())
<h1>Todos los PreEstrenos</h1>
{{ HTML::link(URL::to('PreEstrenosW/create'), 'Agregar nuevo preEstreno') }}
<!-- will be used to show any messages -->
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <td>Id</td>
        <td>Pelicula_id</td>
        <td>Fecha_Estreno</td>
    </tr>
    </thead>
    <tbody>
    @foreach($PreEstrenos as $key => $value)
        <tr>
            <td>{{ $value->id }}</td>
            <td>{{ $value->pelicula_id }}</td>
            <td>{{ $value->fecha_Estreno }}</td>
            <!-- we will also add show, edit, and delete buttons -->
            <td>
                <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                <!-- we will add this later since its a little more complicated than the other two buttons -->
                {{ Form::open(array('url' => 'PreEstrenosW/' . $value->id, 'class' => 'pull-right')) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                {{ Form::close() }}
                <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                <a class="btn btn-small btn-success" href="{{ URL::to('PreEstrenosW/' . $value->id) }}">Show</a>

                <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                <a class="btn btn-small btn-info" href="{{ URL::to('PreEstrenosW/' . $value->id . '/edit') }}">Edit</a>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@else
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-danger">
                    Primero debe iniciar sesion
                </h3>
            </div>
        </div>
    </div>
@endif
@include("../inc/footer")