<!DOCTYPE html>
<html>
<head>
    <title>Editar preestreno</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
@if(Auth::check())
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('PreEstrenosW') }}">PreEstrenos</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('PreEstrenosW') }}">Ver todos los preEstrenos</a></li>
            <li><a href="{{ URL::to('PreEstrenosW/create') }}">Agregar un preEstrenos</a>
        </ul>
    </nav>

    <h1>Edit {{$preestrenos_detail->pelicula_id }}</h1>

    <!-- if there are creation errors, they will show here -->
    {{ HTML::ul($errors->all()) }}

    {{ Form::model($preestrenos_detail, array('route' => array('PreEstrenosW.update', $preestrenos_detail->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('pelicula_id', 'Pelicula_id') }}
        {{ Form::text('pelicula_id', null, array('class' => 'form-control')) }}
        {{ Form::label('fecha_Estreno', 'Fecha_Estreno') }}
        {{ Form::text('fecha_Estreno', null, array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Terminar de editar!', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>
</body>
@else
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-danger">
                    Primero debe iniciar sesion
                </h3>
            </div>
        </div>
    </div>
@endif
</html>