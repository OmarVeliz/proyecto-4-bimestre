<!DOCTYPE html>
<html>
<head>
    <title>Mostrar preEstreno</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
@if(Auth::check())
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('PreEstrenosW') }}">PreEstrenos</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('PreEstrenosW') }}">Ver todos los preEstrenos</a></li>
            <li><a href="{{ URL::to('PreEstrenosW/create') }}">Agregar un preEstreno</a>
        </ul>
    </nav>

    <h1>Showing {{ $preestrenos_detail->pelicula_id }}</h1>

    <div class="jumbotron text-center">
        <h2>{{$preestrenos_detail->pelicula_id }}</h2>
        <p>
            <strong>Direccion:</strong> {{ $preestrenos_detail->fecha_Estreno }}<br>
        </p>
    </div>

</div>
</body>
@else
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-danger">
                    Primero debe iniciar sesion
                </h3>
            </div>
        </div>
    </div>
@endif
</html>