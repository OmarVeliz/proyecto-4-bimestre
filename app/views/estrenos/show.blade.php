<!DOCTYPE html>
<html>
<head>
    <title>Mostrar los estrenos</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
@if(Auth::check())
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('EstrenosW') }}">Estrenos</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('EstrenosW') }}">Ver todos estrenos</a></li>
            <li><a href="{{ URL::to('EstrenosW/create') }}">Agregar estreno</a>
        </ul>
    </nav>

    <h1>Showing {{ $estrenos_detail->fecha_inicio }}</h1>

    <div class="jumbotron text-center">
        <h2>{{$estrenos_detail->fecha_inicio }}</h2>
        <p>
            <strong>Direccion:</strong> {{ $estrenos_detail->fecha_final }}<br>
        </p>
    </div>

</div>
</body>
@else
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-danger">
                    Primero debe iniciar sesion
                </h3>
            </div>
        </div>
    </div>
@endif
</html>