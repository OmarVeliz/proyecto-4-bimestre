<!DOCTYPE html>
<html>
<head>
    <title>Agregar nuevo estreno</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
@if(Auth::check())
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('EstrenosW') }}">Estrenps</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('EstrenosW') }}">Ver todos los estrenos</a></li>
            <li><a href="{{ URL::to('EstrenosW/create') }}">Agregar un estreno</a>
        </ul>
    </nav>

    <h1>Agregar un estreno</h1>

    <!-- if there are creation errors, they will show here -->
    {{ HTML::ul($errors->all()) }}

    {{ Form::open(array('url' => 'EstrenosW')) }}

    <div class="form-group">
        {{ Form::label('cartelera_id', 'Cartelera_id') }}
        {{ Form::text('cartelera_id', Input::old('cartelera_id'), array('class' => 'form-control')) }}
        {{ Form::label('fecha_inicio', 'Fecha_inicio') }}
        {{ Form::text('fecha_inicio', Input::old('fecha_inicio'), array('class' => 'form-control')) }}
        {{ Form::label('fecha_final', 'Fecha_final') }}
        {{ Form::text('fecha_final', Input::old('fecha_final'), array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Agregar estreno!', array('class' => 'btn btn-primary')) }}
    {{ Form::close() }}

</div>
</body>
@else
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-danger">
                    Primero debe iniciar sesion
                </h3>
            </div>
        </div>
    </div>
@endif
</html>