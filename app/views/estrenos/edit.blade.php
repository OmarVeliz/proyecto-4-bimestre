<!DOCTYPE html>
<html>
<head>
    <title>Editar estreno</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
@if(Auth::check())
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('EstrenosW') }}">Estrenos</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('EstrenosW') }}">Ver todos los estrenos</a></li>
            <li><a href="{{ URL::to('EstrenosW/create') }}">Agregar un estreno</a>
        </ul>
    </nav>

    <h1>Edit {{$estrenos_detail->nombre }}</h1>

    <!-- if there are creation errors, they will show here -->
    {{ HTML::ul($errors->all()) }}

    {{ Form::model($estrenos_detail, array('route' => array('EstrenosW.update', $estrenos_detail->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('cartelera_id', 'Cartelera_id') }}
        {{ Form::text('cartelera_id',null, array('class' => 'form-control')) }}
        {{ Form::label('fecha_inicio', 'Fecha_inicio') }}
        {{ Form::text('fecha_inicio', null, array('class' => 'form-control')) }}
        {{ Form::label('fecha_final', 'Fecha_final') }}
        {{ Form::text('fecha_final', null, array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Terminar de editar!', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>
</body>
@else
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-danger">
                    Primero debe iniciar sesion
                </h3>
            </div>
        </div>
    </div>
@endif
</html>