<!DOCTYPE html>
<html>
<head>
    <title>Mostrar salas</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
@if(Auth::check())
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('SalaW') }}">Sala</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('SalaW') }}">Ver todas las salas</a></li>
            <li><a href="{{ URL::to('SalaW/create') }}">Agregar una sala</a>
        </ul>
    </nav>

    <h1>Showing {{ $sala_detail->numero }}</h1>

    <div class="jumbotron text-center">
        <h2>{{$sala_detail->numero }}</h2>
        <p>
            <strong>Tipo:</strong> {{ $sala_detail->tiposala_id }}<br>
        </p>
    </div>

</div>
</body>
@else
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-danger">
                    Primero debe iniciar sesion
                </h3>
            </div>
        </div>
    </div>
@endif
</html>