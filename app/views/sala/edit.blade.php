<!DOCTYPE html>
<html>
<head>
    <title>Editar sala</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
@if(Auth::check())
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('SalaW') }}">Sala</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('SalaW') }}">Ver todas las salas</a></li>
            <li><a href="{{ URL::to('SalaW/create') }}">Agregar una sala</a>
        </ul>
    </nav>

    <h1>Edit {{$formato_detail->nombre }}</h1>

    <!-- if there are creation errors, they will show here -->
    {{ HTML::ul($errors->all()) }}

    {{ Form::model($formato_detail, array('route' => array('SalaW.update', $formato_detail->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('nombre', 'Nombre') }}
        {{ Form::text('nombre', null, array('class' => 'form-control')) }}
        {{ Form::label('descripcion', 'Descripcion') }}
        {{ Form::text('descripcion', null, array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Terminar de editar!', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>
</body>
@else
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-danger">
                    Primero debe iniciar sesion
                </h3>
            </div>
        </div>
    </div>
@endif
</html>