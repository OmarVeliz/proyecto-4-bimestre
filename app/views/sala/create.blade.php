<!DOCTYPE html>
<html>
<head>
    <title>Agregar nueva sala</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
@if(Auth::check())
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('SalaW') }}">Salas</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('SalaW') }}">Ver todas las salas</a></li>
            <li><a href="{{ URL::to('SalaW/create') }}">Agregar una nueva sala</a>
        </ul>
    </nav>

    <h1>Agregar una sala</h1>

    <!-- if there are creation errors, they will show here -->
    {{ HTML::ul($errors->all()) }}

    {{ Form::open(array('url' => 'SalaW')) }}

    <div class="form-group">
        {{ Form::label('cine_id', 'Cine') }}
        {{ Form::text('cine_id', Input::old('cine_id'), array('class' => 'form-control')) }}
        {{ Form::label('numero', 'Numero') }}
        {{ Form::text('numero', Input::old('numero'), array('class' => 'form-control')) }}
        {{ Form::label('tiposala_id', 'Tipo sala') }}
        {{ Form::text('tiposala_id', Input::old('tiposala_id'), array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Agregar sala!', array('class' => 'btn btn-primary')) }}
    {{ Form::close() }}

</div>
</body>
@else
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-danger">
                    Primero debe iniciar sesion
                </h3>
            </div>
        </div>
    </div>
@endif
</html>