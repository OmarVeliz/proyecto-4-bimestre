@include("../inc/header")

@if(Auth::check())
<h1>Todas las peliculas</h1>
{{ HTML::link(URL::to('PeliculasW/create'), 'Agregar nueva pelicula') }}
<!-- will be used to show any messages -->
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <td>id</td>
        <td>Titulo</td>
        <td>Sinopsis</td>
        <td>Trailer</td>
        <td>Image</td>
        <td>Rated</td>
        <td>Genero</td>
    </tr>
    </thead>
    <tbody>
    @foreach($Pelicula as $key => $value)
        <tr>
            <td>{{ $value->id }}</td>
            <td>{{ $value->titulo }}</td>
            <td>{{ $value->sinopsis }}</td>
            <td>
                <iframe width="350" height="350" src="{{$value->trailer_url}}" frameborder="0" allowfullscreen></iframe>
            </td>
            <td>
            <img src="{{ $value -> image}}" width="125" height="125"/>
            </td>
            <td>{{ $value->rated }}</td>
            <td>{{ $value->genero }}</td>
            <!-- we will also add show, edit, and delete buttons -->
            <td>
                <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                <!-- we will add this later since its a little more complicated than the other two buttons -->
                {{ Form::open(array('url' => 'PeliculasW/' . $value->id, 'class' => 'pull-right')) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                {{ Form::close() }}
                <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                <a class="btn btn-small btn-success" href="{{ URL::to('PeliculasW/' . $value->id) }}">Show</a>

                <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                <a class="btn btn-small btn-info" href="{{ URL::to('PeliculasW/' . $value->id . '/edit') }}">Edit</a>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@else
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-danger">
                    Primero debe iniciar sesion
                </h3>
            </div>
        </div>
    </div>
@endif
@include("../inc/footer")