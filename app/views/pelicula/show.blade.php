<!DOCTYPE html>
<html>
<head>
    <title>Mostrar todas las peliculas</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
@if(Auth::check())
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('PeliculasW') }}">Peliculas</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('PeliculasW') }}">Ver todas las peliculas</a></li>
            <li><a href="{{ URL::to('PeliculasW/create') }}">Agregar una nueva pelicula</a>
        </ul>
    </nav>

    <h1>Showing {{ $pelicula_detail->titulo }}</h1>

    <div class="jumbotron text-center">
        <h2>{{$pelicula_detail->nombre }}</h2>
        <p>
            <strong>Direccion:</strong> {{ $pelicula_detail->sinopsis }}<br>
        </p>
    </div>

</div>
</body>
@else
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-danger">
                    Primero debe iniciar sesion
                </h3>
            </div>
        </div>
    </div>
@endif
</html>