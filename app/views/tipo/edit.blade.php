<!DOCTYPE html>
<html>
<head>
    <title>Editar tipo</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
@if(Auth::check())
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('TipoSalaW') }}">Formatos</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('TipoSalaW') }}">Ver todos los formatos</a></li>
            <li><a href="{{ URL::to('TipoSalaW/create') }}">Agregar un formato</a>
        </ul>
    </nav>

    <h1>Edit {{$tipo_detail->nombre }}</h1>

    <!-- if there are creation errors, they will show here -->
    {{ HTML::ul($errors->all()) }}

    {{ Form::model($tipo_detail, array('route' => array('TipoSalaW.update', $tipo_detail->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('nombre', 'Nombre') }}
        {{ Form::text('nombre', null, array('class' => 'form-control')) }}
        {{ Form::label('descripcion', 'Descripcion') }}
        {{ Form::text('descripcion', null, array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Terminar de editar!', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>
</body>
@else
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-danger">
                    Primero debe iniciar sesion
                </h3>
            </div>
        </div>
    </div>
@endif
</html>