<!DOCTYPE html>
<html>
<head>
    <title>Cines</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            @if(Auth::check())
                <a class="navbar-brand" href="{{ URL::to('CineW') }}">Ver cines</a>
            @endif
        </div>
        <ul class="nav navbar-nav">
            @if(!Auth::check())
                <li>{{ HTML::link('user/register', 'Register') }}</li>
                <li>{{ HTML::link('user/login', 'Login') }}</li>
            @else
                <li>{{ HTML::link('CineW/create', 'Cines') }}</li>
                <li>{{ HTML::link('FormatoPeliculaW/create', 'Formatos') }}</li>
                <li>{{ HTML::link('TipoSalaW/create', 'Tipo sala') }}</li>
                <li>{{ HTML::link('SalaW/create', 'Salas') }}</li>
                <li>{{ HTML::link('PeliculasW/create', 'Peliculas') }}</li>
                <li>{{ HTML::link('CarteleraW/create', 'Cartelera') }}</li>
                <li>{{ HTML::link('EstrenosW/create', 'Estrenos') }}</li>
                <li>{{ HTML::link('PreEstrenosW/create', 'PreEstrenos') }}</li>
                <li>         </li>
                    <!--<li>{{ HTML::link('user/logout', 'Logout['.Auth::user()->email.']') }}</li>-->
                <li><a href="{{ URL::to('user/logout') }}">Cerrar sesion [<?php echo Auth::user()->email; ?>]</a>
            @endif
        </ul>
    </nav>
