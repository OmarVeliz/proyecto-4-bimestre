<!DOCTYPE html>
<html>
<head>
    <title>Agregar nuevo formato</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
@if(Auth::check())
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('FormatoPeliculaW') }}">Formatos</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('FormatoPeliculaW') }}">Ver todos los formatos</a></li>
            <li><a href="{{ URL::to('FormatoPeliculaW/create') }}">Agregar un nuevo formato</a>
        </ul>
    </nav>

    <h1>Agregar un formato</h1>

    <!-- if there are creation errors, they will show here -->
    {{ HTML::ul($errors->all()) }}

    {{ Form::open(array('url' => 'FormatoPeliculaW')) }}

    <div class="form-group">
        {{ Form::label('nombre', 'Nombre') }}
        {{ Form::text('nombre', Input::old('nombre'), array('class' => 'form-control')) }}
        {{ Form::label('descripcion', 'Descripcion') }}
        {{ Form::text('descripcion', Input::old('descripcion'), array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Agregar formato!', array('class' => 'btn btn-primary')) }}
    {{ Form::close() }}

</div>
</body>
@else
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-danger">
                    Primero debe iniciar sesion
                </h3>
            </div>
        </div>
    </div>
@endif
</html>