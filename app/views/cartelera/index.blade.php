@include("../inc/header")

@if(Auth::check())
<h1>Todas las carteleras</h1>
{{ HTML::link(URL::to('CarteleraW/create'), 'Agregar nueva cartelera') }}
<!-- will be used to show any messages -->
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <td>Sala_id</td>
        <td>Pelicula_id</td>
        <td>Formatopelicula_id</td>
        <td>Formato_lenguaje</td>
        <td>Fecha</td>
        <td>Hora</td>
    </tr>
    </thead>
    <tbody>
    @foreach($Cartelera as $key => $value)
        <tr>
            <td>{{ $value->sala_id }}</td>
            <td>{{ $value->pelicula_id }}</td>
            <td>{{ $value->formatopelicula_id }}</td>
            <td>{{ $value->formato_lenguaje }}</td>
            <td>{{ $value->fecha }}</td>
            <td>{{ $value->hora }}</td>
            <!-- we will also add show, edit, and delete buttons -->
            <td>
                <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                <!-- we will add this later since its a little more complicated than the other two buttons -->
                {{ Form::open(array('url' => 'CarteleraW/' . $value->id, 'class' => 'pull-right')) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                {{ Form::close() }}
                <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                <a class="btn btn-small btn-success" href="{{ URL::to('CarteleraW/' . $value->id) }}">Show</a>

                <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                <a class="btn btn-small btn-info" href="{{ URL::to('CarteleraW/' . $value->id . '/edit') }}">Edit</a>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@else
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-danger">
                    Primero debe iniciar sesion
                </h3>
            </div>
        </div>
    </div>
@endif
@include("../inc/footer")