<!DOCTYPE html>
<html>
<head>
    <title>Agregar nueva cartelera</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
@if(Auth::check())
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('CarteleraW') }}">Cartelera</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('CarteleraW') }}">Ver todas las carteleras</a></li>
            <li><a href="{{ URL::to('CarteleraW/create') }}">Agregar una cartelera</a>
        </ul>
    </nav>

    <h1>Agregar una cartelera</h1>

    <!-- if there are creation errors, they will show here -->
    {{ HTML::ul($errors->all()) }}

    {{ Form::open(array('url' => 'CarteleraW')) }}

    <div class="form-group">
        {{ Form::label('sala_id', 'Sala id') }}
        {{ Form::text('sala_id', Input::old('sala_id'), array('class' => 'form-control')) }}
        {{ Form::label('pelicula_id', 'Pelicula id') }}
        {{ Form::text('pelicula_id', Input::old('pelicula_id'), array('class' => 'form-control')) }}
        {{ Form::label('formatopelicula_id', 'Formatopelicula id') }}
        {{ Form::text('formatopelicula_id', Input::old('formatopelicula_id'), array('class' => 'form-control')) }}
        {{ Form::label('formato_lenguaje', 'Formato_lenguaje') }}
        {{ Form::text('formato_lenguaje', Input::old('formato_lenguaje'), array('class' => 'form-control')) }}
        {{ Form::label('fecha', 'Fecha') }}
        {{ Form::text('fecha', Input::old('fecha'), array('class' => 'form-control')) }}
        {{ Form::label('hora', 'Hora') }}
        {{ Form::text('hora', Input::old('hora'), array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Agregar cartelera!', array('class' => 'btn btn-primary')) }}
    {{ Form::close() }}

</div>
</body>
@else
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-danger">
                    Primero debe iniciar sesion
                </h3>
            </div>
        </div>
    </div>
@endif
</html>