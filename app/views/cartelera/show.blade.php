<!DOCTYPE html>
<html>
<head>
    <title>Mostrar carteleras</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
@if(Auth::check())
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('CarteleraW') }}">Carteleras</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('CarteleraW') }}">Ver todas las carteleras</a></li>
            <li><a href="{{ URL::to('CarteleraW/create') }}">Agregar cartelera</a>
        </ul>
    </nav>

    <h1>Showing {{ $cartelera_detail->formato_lenguaje }}</h1>

    <div class="jumbotron text-center">
        <h2>{{$cartelera_detail->formato_lenguaje }}</h2>
        <p>
            <strong>Direccion:</strong> {{ $cartelera_detail->fecha }}<br>
        </p>
    </div>

</div>
</body>
@else
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-danger">
                    Primero debe iniciar sesion
                </h3>
            </div>
        </div>
    </div>
@endif
</html>