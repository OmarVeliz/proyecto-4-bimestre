<!DOCTYPE html>
<html>
<head>
    <title>Editar cartelera</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
@if(Auth::check())
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('CarteleraW') }}">Carteleras</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('CarteleraW') }}">Ver todos los formatos</a></li>
            <li><a href="{{ URL::to('CarteleraW/create') }}">Agregar un formato</a>
        </ul>
    </nav>

    <h1>Edit {{$cartelera_detail->nombre }}</h1>

    <!-- if there are creation errors, they will show here -->
    {{ HTML::ul($errors->all()) }}

    {{ Form::model($cartelera_detail, array('route' => array('CarteleraW.update', $cartelera_detail->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('sala_id', 'Sala id') }}
        {{ Form::text('sala_id', null, array('class' => 'form-control')) }}
        {{ Form::label('pelicula_id', 'Pelicula id') }}
        {{ Form::text('pelicula_id', null, array('class' => 'form-control')) }}
        {{ Form::label('formatopelicula_id', 'Formatopelicula id') }}
        {{ Form::text('formatopelicula_id', null, array('class' => 'form-control')) }}
        {{ Form::label('formato_lenguaje', 'Formato_lenguaje') }}
        {{ Form::text('formato_lenguaje', null, array('class' => 'form-control')) }}
        {{ Form::label('fecha', 'Fecha') }}
        {{ Form::text('fecha', null, array('class' => 'form-control')) }}
        {{ Form::label('hora', 'Hora') }}
        {{ Form::text('hora',null, array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Terminar de editar!', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>
</body>
@else
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-danger">
                    Primero debe iniciar sesion
                </h3>
            </div>
        </div>
    </div>
@endif
</html>