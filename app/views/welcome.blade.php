@include("../inc/header")

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h3 class="text-center">
               Cine Kinal.
            </h3>
            <div class="carousel slide" id="carousel-711687">
                <ol class="carousel-indicators">
                    <li class="active" data-slide-to="0" data-target="#carousel-711687">
                    </li>
                    <li data-slide-to="1" data-target="#carousel-711687">
                    </li>
                    <li data-slide-to="2" data-target="#carousel-711687">
                    </li>
                </ol>
                <div class="carousel-inner">
                    <div class="item active">
                        <img alt="Carousel Bootstrap First" src="http://lorempixel.com/output/sports-q-c-1600-500-1.jpg" />
                        <div class="carousel-caption">
                            <h4>
                                Primera imagen
                            </h4>
                            <p>
                                Algo que recordar.
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <img alt="Carousel Bootstrap Second" src="http://lorempixel.com/output/sports-q-c-1600-500-2.jpg" />
                        <div class="carousel-caption">
                            <h4>
                                Segunda imagen
                            </h4>
                            <p>
                                Una pelicula indescribible
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <img alt="Carousel Bootstrap Third" src="http://lorempixel.com/output/sports-q-c-1600-500-3.jpg" />
                        <div class="carousel-caption">
                            <h4>
                                Tercera imagen
                            </h4>
                            <p>
                                Un mundo nuevo
                            </p>
                        </div>
                    </div>
                </div> <a class="left carousel-control" href="#carousel-711687" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#carousel-711687" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
            <div class="jumbotron">
                <h2>
                    Bienvenido!
                </h2>
                <p>
                    Le ofrecemos un servicio de calidad .
                </p>
                <p>
                    <a class="btn btn-primary btn-large" href="#">Learn more</a>
                </p>
            </div>
        </div>
    </div>
</div>

@include("../inc/footer")